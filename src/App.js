import React,{useState,useEffect} from 'react';

import {BrowserRouter as Router} from 'react-router-dom';
import {Route, Switch} from 'react-router-dom';

import AppNavBar from './components/AppNavBar';
import './App.css';
import {Container} from 'react-bootstrap';

import {UserProvider} from './userContext';

//import pages:
import Home from './pages/Home';
import Login from './pages/Login';
import Register from './pages/Register';
import Logout from './pages/Logout';
import Products from './pages/Products';
import ViewProduct from './pages/ViewProduct';
import ViewCategory from './pages/ViewCategory';
import AddProduct from './pages/AddProduct';
import ModifyProduct from './pages/ModifyProduct';
import Users from './pages/Users';
import UserOrders from './pages/UserOrders';
import NotFound from './pages/NotFound';



export default function App() {

    const [user,setUser] = useState({

      id: null,
      isAdmin: null

    })

  //fetch user's details:
  useEffect(()=>{

    fetch('https://immense-brushlands-59734.herokuapp.com/users/viewUser',{

      headers: {
        'Authorization': `Bearer ${localStorage.getItem('token')}`
      }

    })
    .then(res => res.json())
    .then(data => {

      setUser({
        id: data._id,
        isAdmin: data.isAdmin
      })

    })

  },[])

  console.log(user)


  //clear localStorage on logout
  const unsetUser = () => { 
    localStorage.clear();
  }


  return (
      <>
        <UserProvider value={{user,setUser,unsetUser}}> 
          <Router>        
          <AppNavBar/>
            <Container>
              <Switch>
                <Route exact path="/" component={Home}/>
                <Route exact path="/login" component={Login}/>
                <Route exact path="/register" component={Register}/>
                <Route exact path="/logout" component={Logout}/> 
                <Route exact path="/products" component={Products}/>
                <Route exact path="/product/add" component={AddProduct}/>
                <Route exact path="/products/:productId" component={ViewProduct}/>
                <Route exact path="/products/category/:category" component={ViewCategory}/>
                <Route exact path="/products/modify/:productId" component={ModifyProduct}/>
                <Route exact path="/users" component={Users}/>
                <Route exact path="/users/:id" component={UserOrders}/>
                <Route component={NotFound}/> 
              </Switch>
            </Container>
          </Router>
        </UserProvider>
      </> 
    )

}


