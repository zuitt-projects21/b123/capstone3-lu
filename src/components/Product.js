import React from 'react';
import {Col,Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

export default function Product({productProp}) {

	return(
		<Col md={5}>
		<Card id="prodCards">
			<Card.Body>
				<Card.Title>{productProp.name}</Card.Title>
				<Card.Subtitle>Description:</Card.Subtitle>
				<Card.Text>{productProp.description}</Card.Text>
				<Card.Subtitle>Price:</Card.Subtitle>
				<Card.Text>PHP {productProp.price}</Card.Text>
				<Link className="btn btn-success" to={`/products/${productProp._id}`}>View Product</Link>
			</Card.Body>
		</Card>
		</Col>

	)

}