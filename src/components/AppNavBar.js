import React,{useContext} from 'react';
import {Navbar,Nav,Row,Col} from 'react-bootstrap';
import {Link} from 'react-router-dom';
import UserContext from '../userContext';

export default function AppNavBar() {

	const {user} = useContext(UserContext);
	console.log(user);


	return (

		<Navbar bg="success" expand="lg">
			<Navbar.Brand as={Link} to="/">
				<i class="fas fa-home mr-1"></i>
				Home.App		
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="basic-navbar-nav"/>
			<Navbar.Collapse id="basic-navbar-nav">
				<Nav className="ml-auto">
					<Nav.Link as={Link} to="/products">
						<i class="fas fa-store-alt mr-1"></i>
						Products
					</Nav.Link>
					{
						user.id
						? 
							user.isAdmin
							? 
								<>
								<Nav.Link as={Link} to="/users">
									<i class="fas fa-users mr-1"></i>
									Users
								</Nav.Link>
								<Nav.Link as={Link} to="/logout">
									<i class="fas fa-sign-out-alt mr-1"></i>
									Logout
								</Nav.Link>
								</>	
							: 
								<>
								<Nav.Link as={Link} to={`/`}>
									<i class="fas fa-shopping-cart mr-1"></i>
									View Cart
								</Nav.Link>
								<Nav.Link as={Link} to={`/users/${user.id}`}>
									<i class="fas fa-dolly-flatbed"></i>
									Orders
								</Nav.Link>
								<Nav.Link as={Link} to="/logout">
									<i class="fas fa-sign-out-alt mr-1"></i>
									Logout
								</Nav.Link>
								</>
						: 
						<>
							<Nav.Link as={Link} to="/register">
								<i class="fas fa-user-plus mr-1"></i>
								Register
							</Nav.Link>
							<Nav.Link as={Link} to="/login">
								<i class="fas fa-sign-in-alt mr-1"></i>
								Login
							</Nav.Link>
						</>
					}
					
				</Nav>
			</Navbar.Collapse>
		</Navbar>

		)

}