import React from 'react';

import {Row,Col,Card} from 'react-bootstrap';
import {Link} from 'react-router-dom';

import logo1 from './images/icons8-drill-96.png';
import logo2 from './images/icons8-paint-roller-96.png';
import logo3 from './images/icons8-screw-96.png';
import logo4 from './images/icons8-ellipsis-96.png';


export default function Categories() {

	//const [category,setCategory] = useState([]);

	return(
		<>
		<h2 className="text-center mt-4 mb-5">Product Categories</h2>
			<Row>	
				<Col md={3} xs={6} className="mb-3">	
					<Card as={Link} to="/products/category/Tools">
						<Card.Img src={logo1} className="catIcon" fluid/>
						<Card.Text className="catText">Tools</Card.Text>
					</Card>
				</Col>
				<Col md={3} xs={6} className="mb-3">	
					<Card as={Link} to="/products/category/Paint">
						<Card.Img src={logo2} className="catIcon" fluid/>
						<Card.Text className="catText">Paint</Card.Text>
					</Card>
				</Col>
				<Col md={3} xs={6} className="mb-3">	
					<Card as={Link} to="/products/category/Materials">
						<Card.Img src={logo3} className="catIcon" fluid/>
						<Card.Text className="catText">Materials</Card.Text>
					</Card>
				</Col>
				<Col md={3} xs={6} className="mb-3">	
					<Card as={Link} to="/products/category/Others">
						<Card.Img src={logo4} className="catIcon" fluid/>
						<Card.Text className="catText">Others</Card.Text>
					</Card>
				</Col>

			</Row>	

		</>
	)
}
