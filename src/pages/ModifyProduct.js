import React,{useState,useEffect,useContext} from 'react';
import {Form, Button} from 'react-bootstrap';
import UnauthorizedAccess from './UnauthorizedAccess';

import Swal from 'sweetalert2'

import UserContext from '../userContext';

import {Redirect,useHistory,useParams} from 'react-router-dom';

export default function ModifyProduct(){

	const{user} = useContext(UserContext);
	const {productId} = useParams();

	const history = useHistory();

	const [name,setName] = useState("");
	const [description,setDescription] = useState("");
	const [price,setPrice] = useState("");
	const [category,setCategory] = useState("");
	const [stock,setStock] = useState("");

	useEffect(()=>{
		if(name !== "" && description !== "" && price !== "" && category !== "" && stock !== ""){
			setIsActive(true)
		} else{
			setIsActive(false)
		}

	},[name,description,price,category,stock])

	const [isActive,setIsActive] = useState(false);

	useEffect(()=>{

		fetch(`https://immense-brushlands-59734.herokuapp.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Product Unavailable",
					text: data.message
				})
			} else{

				setName(data.name);
				setDescription(data.description);
				setCategory(data.category);
				setStock(data.stock);
				setPrice(data.price);

			}

		})

	},[productId])



	function modProduct(e){
		e.preventDefault();

		fetch(`https://immense-brushlands-59734.herokuapp.com/products/update/${productId}`,{

			method: 'PUT',
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${localStorage.getItem("token")}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				category: category,
				stock: stock,
				price: price 
			})
		})
		.then(res => res.json())
		.then(data => {
			if(data.name){
				Swal.fire({
					icon: "success",
					title: `Product: ${data.name} Successfully Modified!`,
					text: `You will be redirected back to the products dashboard.`
				})

				history.push('/products')
			} else {
				Swal.fire({
					icon: "error",
					title: "Failed to modify product.",
					text: data.message
				})
			}	
		})

	}

	/*
		<Form.Group>
			<Form.Label>Category:</Form.Label>
			<Form.Control type="text" value={category} onChange={e => {setCategory(e.target.value)}} required/>
		</Form.Group>
	*/

	return (
		user.isAdmin
		? 
			<>
				<h1 className="my-5 text-center">Modify Product</h1>
				<Form onSubmit={e => modProduct(e)}>
					<Form.Group>
						<Form.Label>Product Name:</Form.Label>
						<Form.Control type="text" value={name} onChange={e => {setName(e.target.value)}} placeholder="Enter Name" required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Description:</Form.Label>
						<Form.Control type="text" value={description} onChange={e => {setDescription(e.target.value)}} placeholder="Enter Description" required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Category:</Form.Label>
						<Form.Control as="select" value={category} onChange={e => {setCategory(e.target.value)}} required>
							<option>Tools</option>
							<option>Paint</option>
							<option>Materials</option>
							<option>Others</option>
						</Form.Control>	
					</Form.Group>
					<Form.Group>
						<Form.Label>Stock:</Form.Label>
						<Form.Control type="number" value={stock} onChange={e => {setStock(e.target.value)}}  placeholder="Enter Number of Stocks" required/>
					</Form.Group>
					<Form.Group>
						<Form.Label>Price:</Form.Label>
						<Form.Control type="number" value={price} onChange={e => {setPrice(e.target.value)}}  placeholder="Enter Product Price" required/>
					</Form.Group>
					{
					isActive 
					? <Button variant="primary" type="submit">Save Changes</Button>
					: <Button variant="secondary" disable>Save Changes</Button>
					}
				</Form>
			</>	
		: <Redirect to="/login"/> 	

	)


}