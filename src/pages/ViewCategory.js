import React,{useState,useEffect} from 'react';
import {Link, useParams} from 'react-router-dom';

import Product from '../components/Product';

import UserContext from '../userContext';

import {Row,Table,Button} from 'react-bootstrap';

export default function ViewCategory() {

	const {category} = useParams()

	const [productsArray, setProductsArray] = useState([]);

	useEffect(()=>{

		fetch(`https://immense-brushlands-59734.herokuapp.com/products/category/${category}`)
		.then(res => res.json())
		.then(data => {

			setProductsArray(data.map(product => {
		
				return (
					<Product key={product._id} productProp={product}/>
				)
					
			}))
		})
	},[])

	return(
		<>
			<h1 className="my-5">Products List - {category}</h1>
			<Row>
				{productsArray}
			</Row>
		</>
	)
}