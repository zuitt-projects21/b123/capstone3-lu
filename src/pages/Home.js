import React,{useContext} from 'react';
import Banner from '../components/Banner';
import Categories from '../components/Categories';

import {Redirect} from 'react-router-dom';

import UserContext from '../userContext';

export default function Home() {

	const{user} = useContext(UserContext);

	let homeProp = {
		title: "Home.App",
		description: "Your home/construction needs in one app.",
		buttonCallToAction: "Browse Items",
		destination: "/products"

	};


	return (
			user.isAdmin
			? <Redirect to="/products"/> 
			:
			<>
				<Banner bannerProp={homeProp}/>
				<Categories/>		
			</>	

		)

}