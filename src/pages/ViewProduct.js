import React,{useState,useEffect,useContext} from 'react';
import {Card,Button,Row,Col} from 'react-bootstrap';
import {useParams,useHistory,Link} from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../userContext';

export default function ViewProduct(){

	const {productId} = useParams();	

	const {user} = useContext(UserContext);
	const history = useHistory();

	const [productDetails,setProductDetails] = useState({

		name: null,
		description: null,
		stock: null,
		price: null

	});

	useEffect(()=>{

		fetch(`https://immense-brushlands-59734.herokuapp.com/products/${productId}`)
		.then(res=>res.json())
		.then(data=>{
			if(data.message){
				Swal.fire({
					icon: "error",
					title: "Product Unavailable",
					text: data.message
				})
			} else{
				setProductDetails({
					name: data.name,
					description: data.description,
					stock: data.stock,
					price: data.price
				})
			}

		})

	},[productId])

	console.log(productDetails)

	return(

			<Row className="mt-5">
				<Col>
					<Card>	
						<Card.Body>
							<Card.Title>{productDetails.name}</Card.Title>
							<Card.Subtitle>Description:</Card.Subtitle>
							<Card.Text>{productDetails.description}</Card.Text>
							<Card.Subtitle>Stocks:</Card.Subtitle>
							<Card.Text>{productDetails.stock}</Card.Text>
							<Card.Subtitle>Price:</Card.Subtitle>
							<Card.Text>PHP {productDetails.price}</Card.Text>
							{
								user.isAdmin === false
								?
									productDetails.stock > 0
									?<Button variant="primary" block>Add to Cart</Button>
									:<Button variant="secondary" block>Out of Stock</Button>

								:
								user.id !== undefined  
								?<Link className="btn btn-danger btn-block" to="/">Not Available for Admin User</Link>
								:
									productDetails.stock > 0
										?<Link className="btn btn-danger btn-block" to="/login">Login to Add to Cart</Link>
										:<Button variant="secondary" block>Out of Stock</Button>
							}

						</Card.Body>	
					</Card>	
				</Col>	
			</Row>	

		)

}