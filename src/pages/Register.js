import React,{useState,useEffect,useContext} from 'react';
import {Row, Col,Form, Button} from 'react-bootstrap';

import UserContext from '../userContext';
import {Redirect,useHistory} from 'react-router-dom';

import Swal from 'sweetalert2';

export default function Register() {

	const {user} = useContext(UserContext);

	const history = useHistory();

	const [firstName,setFirstName] = useState("");	
	const [lastName,setLastName] = useState("");	
	const [email,setEmail] = useState("");	
	const [mobileNo,setMobileNo] = useState("");
	const [address, setAddress]	= useState("");
	const [password,setPassword] = useState("");	
	const [confirmPassword,setConfirmPassword] = useState("");

	const [isActive,setIsActive] = useState(false);	 



	useEffect(()=>{

		if((firstName !== "" && lastName !== "" && email !== "" && mobileNo !== "" && address !== "" && password !== "" && confirmPassword !== "") && (password === confirmPassword) &&(mobileNo.length === 11)){
			setIsActive(true)
		} else {
			setIsActive(false)
		}

	},[firstName,lastName,email,mobileNo,address,password,confirmPassword])

	function registerUser(e){
		e.preventDefault();

		fetch('https://immense-brushlands-59734.herokuapp.com/users/register/',{
			method: "POST",
			headers: {
				"Content-Type": "application/json"
			},
			body: JSON.stringify({
				firstName: firstName,
				lastName: lastName,
				email: email,
				mobileNo: mobileNo,
				address: address,
				password: password
			})
		})
		.then(res=>res.json())
		.then(data=>{
			if (data.message === "Registered Successfully"){
				Swal.fire({
					icon: "success",
					title: "Registration Successful!",
					text: "Thank you for registering!"
				})
				history.push('/login')
			} else{
				Swal.fire({
					icon: "error",
					title: "Registration Failed.",
					text: data.message
				})
			}
		})

	}

	return(
		user.id
		? <Redirect to="/"/>
		: 
			<>
			<h1 className="my-5 text-center">Register</h1>
			<Row className="justify-content-md-center">
				<Col md={8}>
					<Form onSubmit={e => registerUser(e)}>
						<Form.Group>
							<Form.Label>First Name:</Form.Label>
							<Form.Control type="text" value={firstName} onChange={e => {setFirstName(e.target.value)}} placeholder="Enter First Name" required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Last Name:</Form.Label>
							<Form.Control type="text" value={lastName} onChange={e => {setLastName(e.target.value)}} placeholder="Enter Last Name" required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Email:</Form.Label>
							<Form.Control type="email" value={email} onChange={e => {setEmail(e.target.value)}} placeholder="Enter Email" required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Mobile No.:</Form.Label>
							<Form.Control type="number" value={mobileNo} onChange={e => {setMobileNo(e.target.value)}}  placeholder="Enter 11 Digit Mobile No." required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Address:</Form.Label>
							<Form.Control as="textarea" rows={3} type="text" value={address} onChange={e => {setAddress(e.target.value)}}  placeholder="Enter Address" required/>
						</Form.Group>
						<Form.Group>
							<Form.Label>Password:</Form.Label>
							<Form.Control type="password" value={password} onChange={e => {setPassword(e.target.value)}}  placeholder="Enter Password" required/>
						</Form.Group>
							<Form.Group>
							<Form.Label>Confirm Password:</Form.Label>
							<Form.Control type="password" value={confirmPassword} onChange={e => {setConfirmPassword(e.target.value)}} placeholder="Confirm Password" required/>
						</Form.Group>

						{
						isActive 
						? <Button variant="primary" type="submit">Submit</Button>
						: <Button variant="secondary" disable>Submit</Button>
						}
					</Form>
				</Col>
			</Row>
			</>
	)

}	