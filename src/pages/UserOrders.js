import React,{useState,useEffect,useContext} from 'react';
import {Link,useParams,Redirect} from 'react-router-dom';

import UserContext from '../userContext';

import {Table,Button} from 'react-bootstrap';

export default function UserOrders(linkProp) {

	const {user} = useContext(UserContext);
	const {id} = useParams()

	//states
	const [ordersArray, setOrdersArray] = useState([]);


	useEffect(()=>{

		if(user.isAdmin){

			fetch(`https://immense-brushlands-59734.herokuapp.com/users/orders/${id}`,{
				headers: {
					Authorization: `Bearer ${localStorage.getItem("token")}`
				}

			})
			.then(res => res.json())
			.then(data => {

				setOrdersArray(data.map((order)=>{

					return(

						<tr key={order._id}>
							<td>{order._id}</td>
							<td>{order.datePurchased}</td>
							<td>{order.totalAmount}</td>
							<td>{order.status}</td>
						</tr>
					)

				}))

			})
		} else if (user.id){
			fetch(`https://immense-brushlands-59734.herokuapp.com/users/orders/`,{
				headers: {
					Authorization: `Bearer ${localStorage.getItem("token")}`
				}

			})
			.then(res => res.json())
			.then(data => {

				setOrdersArray(data.map((order)=>{

					return(

						<tr key={order._id}>
							<td>{order._id}</td>
							<td>{order.datePurchased}</td>
							<td>{order.totalAmount}</td>
							<td>{order.status}</td>
						</tr>
					)

				}))

			})
		} 

	},[])

	return(
		user.id
		?
			<>
				{
				user.isAdmin
				? <h1 className="my-5">Orders for {id}</h1>
				: <h1 className="my-5">Orders</h1>
				}
				<Table striped bordered hover responsive>
					<thead>
						<tr>
							<th>Order ID</th>
							<th>Date Purchased</th>
							<th>Total Amount</th>
							<th>Status</th>
						</tr>
					</thead>
					<tbody>
						{ordersArray}
					</tbody>
				</Table>
			</>
		: <Redirect to="/"/>

	)
}