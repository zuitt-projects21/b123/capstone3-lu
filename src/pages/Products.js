import React,{useState,useEffect,useContext} from 'react';
import {Link} from 'react-router-dom';

import Product from '../components/Product';

import UserContext from '../userContext';

import {Row,Col,Table,Button} from 'react-bootstrap';


export default function Products() {

	const {user} = useContext(UserContext);

	//states
	const [productsArray, setProductsArray] = useState([]);
	const [allProducts,setAllProducts] = useState([]);
	const [update,setUpdate] = useState(0);

	useEffect(()=>{

		fetch("https://immense-brushlands-59734.herokuapp.com/products/")
		.then(res => res.json())
		.then(data => {
			
			setProductsArray(data.map(product => {
		
				return (
					<Product key={product._id} productProp={product}/>
				)
					
			}))
		})
	},[])

	useEffect(()=>{

		if(user.isAdmin){

			fetch("https://immense-brushlands-59734.herokuapp.com/products/view/all",{
				headers: {
					"Authorization": `Bearer ${localStorage.getItem("token")}` 
				}
			})
			.then(res=>res.json())
			.then(data => {
				//console.log(data)
				setAllProducts(data.map((product)=>{

					return (

						<tr key={product._id}>
							<td>{product.name}</td>
							<td>{product.stock}</td>
							<td>PHP {product.price}</td>
							<td>{product.isActive ? "Active" : "Inactive"}</td>
							<td>
								{
									product.isActive
									?
									 <Button variant='danger' className='mx-2 my-1' onClick={()=>{
									 		archive(product._id)

									 	 }}>Archive</Button>			 
									:	
									<Button variant='success' className='mx-2 my-1' onClick={()=>{
									 		activate(product._id)

									 	 }}>Activate</Button>
								}

								<Link to={`/products/modify/${product._id}`} className='btn btn-warning mx-2 my-1'>Modify</Link>							
							</td>
						</tr>

					)

				}))
			})

		}

	},[user,update])


	function archive(productId){
		console.log(productId)
		fetch(`https://immense-brushlands-59734.herokuapp.com/products/archive/${productId}`,{
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			//inser
			setUpdate(update+1)
		})
	}

	function activate(productId){
		console.log(productId)
		fetch(`https://immense-brushlands-59734.herokuapp.com/products/activate/${productId}`,{
			method: 'PUT',
			headers: {
				Authorization: `Bearer ${localStorage.getItem("token")}`
			}
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)
			setUpdate(update+1)
		})
	}	

	return(
		user.isAdmin
		?
			<>
				<h1 className="text-center mt-5">Products Dashboard</h1>
				<Row className="justify-content-center">
				<Link className="btn btn-warning mb-4" to={"/product/add"}>Add Product</Link>
				</Row>
				<Table striped bordered hover responsive>
					<thead>
						<tr>
							<th>Name</th>
							<th>Stocks</th>
							<th>Price</th>
							<th>Status</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
						{allProducts}
					</tbody>
				</Table>
			</>
		:
		<>
		<h1 className="my-5">Products List</h1>
		<Row>
			{productsArray}
		</Row>
		</>
	)


}	