import React from 'react';
import Banner from '../components/Banner';

export default function NotFound() {
	let errorProp = {

		title: "404: Page not Found",
		description: "",
		buttonCallToAction: "Go Back to Home Page",
		destination: "/"

	};

	return(
		<>
			<Banner bannerProp={errorProp} />
		</>
	)

}
