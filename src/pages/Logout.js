import React,{useContext,useEffect} from 'react';
import UserContext from '../userContext';
import Banner from '../components/Banner';

export default function Logout(){
	const {setUser,unsetUser} = useContext(UserContext);
	
	unsetUser();

	useEffect(()=>{
			//set the global user state to its initial values
			//updating a state uncluded in the context with its setter function.
			setUser({
				id:null,
				isAdmin:null
			})

	},[])

	let bannerContent = {

		title: "Logout Successful",
		description: "Please come again!",
		buttonCallToAction: "Go Back to Home Page",
		destination: "/"

	}; 	

	return(
		<Banner bannerProp={bannerContent}/>	
	)

}