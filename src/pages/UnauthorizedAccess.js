import React from 'react';
import Banner from '../components/Banner';

export default function UnauthorizedAccess() {
	let errorProp = {

		title: "Unauthorized Access",
		description: "The page you are trying to access is unavailable",
		buttonCallToAction: "Back to Products",
		destination: "/products"

	};

	return(
		<>
			<Banner bannerProp={errorProp} />
		</>
	)

}
