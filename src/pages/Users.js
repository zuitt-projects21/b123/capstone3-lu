import React,{useState,useEffect,useContext} from 'react';
import {Link,Redirect} from 'react-router-dom';

import UserContext from '../userContext';

import {Table,Button} from 'react-bootstrap';

export default function Users() {

	const {user} = useContext(UserContext);

	//states
	const [usersArray, setUsersArray] = useState([]);


	useEffect(()=>{

		if(user.isAdmin){

			fetch("https://immense-brushlands-59734.herokuapp.com/users/allOrders",{
				headers: {
					Authorization: `Bearer ${localStorage.getItem("token")}`
				}

			})
			.then(res => res.json())
			.then(data => {

				setUsersArray(data.map((user)=>{

					return(

						<tr key={user._id}>
							<td>{user.firstName} {user.lastName}</td>
							<td>{user.email}</td>
							<td>{user.mobileNo}</td>
							<td>{user.orders.length}</td>
							<td>
								<Link to={`/users/${user._id}`} className='btn btn-success mx-2 my-1'>View Orders</Link>							
							</td>
						</tr>
					)

				}))

			})
		}

	},[])

	return(
		user.isAdmin
		?
		<>
			<h1 className="text-center my-5">Users Dashboard</h1>
			<Table striped bordered hover responsive>
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Contact No.</th>
						<th>Orders</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{usersArray}
				</tbody>
			</Table>
		</>
		: <Redirect to="/"/>

	)
}