import React from 'react';

/*
	Create a Context Object
		A context is a special react object which will allow us to store information within and passes it around our components within the app.

		The context object is a different approach to passing information between components without the need to pass props from component to component.

*/

const UserContext = React.createContext();

/*
	The Provider component is what allows other components to consume or use our context. Any component which is not wrapped by our Provider will not have access to the values provided for our context.
*/

export const UserProvider = UserContext.Provider;

export default UserContext;